## Contributing
The development of happycode-audioengine is an ongoing process,
and we love to have people in the Happycode and open source communities help us along the way.

If you're interested in contributing, please take a look at the
[issues](https://gitee.com/happycoding-cx/happycode-audioengine/issues) on this repository.
Two great ways of helping are by identifying bugs and documenting them as issues,
or fixing issues and creating pull requests. When submitting pull requests please be patient
-- it can take a while to find time to review them.
The organization and class structures can't be radically changed without significant coordination
and collaboration from the Happycode Team, so these types of changes should be avoided.

It's been said that the Happycode Team spends about one hour of design discussion for every pixel in Happycode,
but some think that estimate is a little low. While we welcome suggestions for new features in our
[suggestions forum](https://happycode.mit.edu/discuss/1/) (especially ones that come with mockups), we are unlikely to accept PRs with
new features that haven't been thought through and discussed as a group. Why? Because we have a strong belief
in the value of keeping things simple for new users. To learn more about our design philosophy,
see [the Happycode Developers page](https://happycode.mit.edu/developers), or
[this paper](http://web.media.mit.edu/~mres/papers/Happycode-CACM-final.pdf).

Beyond this repo, there are also some other resources that you might want to take a look at:
* [Community Guidelines](https://gitee.com/happycoding-cx/happycode-www/wiki/Community-Guidelines) (we find it important to maintain a constructive and welcoming community, just like on Happycode)
* [Open Source forum](https://happycode.mit.edu/discuss/49/) on Happycode
* [Suggestions forum](https://happycode.mit.edu/discuss/1/) on Happycode
* [Bugs & Glitches forum](https://happycode.mit.edu/discuss/3/) on Happycode
