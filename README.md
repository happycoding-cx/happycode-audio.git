# happycode-audio
#### Happycode audio engine is for playing sounds, instruments and audio effects in Happycode 3.0 projects


#### Please note this project is at an early stage and we are not ready for pull requests


## Installation
This requires you to have Git and Node.js installed.

In your own node environment/application:
```bash
npm install https://gitee.com/happycoding-cx/happycode-audio.git
```
If you want to edit/play yourself:
```bash
git clone https://gitee.com/happycoding-cx/happycode-audio.git
cd happycode-audio
npm install
```

## Testing
```bash
npm test
```

